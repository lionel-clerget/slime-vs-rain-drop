# Slime vs Rain Drop
Intégration d'un jeux en JavaScript, intéraction front & développement d'intelligence artificielle

## Objectifs
* Créer une maquette format mobile
* Implémenter des méthodes JS d'un jeux existant
* Créer les intéractions front
* Développer l'intelligence artificielle
