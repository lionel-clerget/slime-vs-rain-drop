function checkLine(array) {
    let arrayLine = array[2];
    for (var i = 0; i < arrayLine.length; i++) {
        if (arrayLine[i] === 1 && game.position === i) {
            if (i === 0 || i === 4) {
                if (i === 0) {
                    // goToRight
                    rightButton();
                }else {
                    // goToLeft
                    leftButton();
                }
                break
            }else {
                let rand = Math.floor(Math.random() * 2);
                if (rand === 0 ) {
                    leftButton();
                }else {
                    rightButton();
                }
            }
        }
    }
}

function getPath(matrix, endPos) {
    easystar.setGrid(matrix);
    easystar.findPath(game.position, 7, endPos, 0, function( path ) {
    	if (path === null) {
    		console.log("Path was not found.");
            // if (lastPlayerPath != null) {
            //     console.log(lastPlayerPath);
            //     checkPath(lastPlayerPath)
            // }
    	} else {
            // playerPath = path;
    		console.log("Path was found. The first Point is " + path[0].x + " " + path[0].y);
            checkPath(path);
            lastPlayerPath = path;
    	}
        console.log(path);
    });
    easystar.calculate();
}

function checkPath(path) {
    for (var i = 0; i < path.length; i++) {
        if (path[i].x < game.position) {
            leftButton();
            console.log(`left & index = ${path[i].x}`);
            return
        }else if (path[i].x > game.position){
            rightButton();
            console.log(`right & index = ${path[i].x}`);
            return
        }
    }
}

function indexOfClosest(nums, target) {
    let closest = Number.MAX_SAFE_INTEGER;
    let index = 0;

    nums.forEach((num, i) => {
        let dist = Math.abs(target - num);
        if (num === 0) {
            if (dist < closest) {
                index = i;
                closest = dist;
            }
        }
    });

  return index;
}
