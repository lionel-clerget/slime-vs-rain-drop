// var left, right, start, modal, message, login;
var game;
var onLine = new SweatyPromoClient.online();
var offLine = new SweatyPromoClient.offline();
var start = document.getElementById('startButton');
var isOnline = false;
// var isOnline = onLine.isAuth();

if (isOnline) {
    game = offLine;
    // game = onLine;
}else {
    game = offLine;
}

switchOnline();
switchBtnInGame();

var lastPlayerPath = null;
var isAi = true;
var isPlayerInLife = true;
var isModalOpen = false;
var currModal = "none";
var isPopMessage = false;
var isPopActive = true;
var isSoundActive = true;
var smallScreen = 1024;

var goutte = {
    imgSrc: {
        default : './src/img/goutte_iddle.svg',
        empty : './src/img/empty.png',
        anim : './src/img/goutte_explode.gif'
    }
}
var slime = {
    elem: document.getElementById(`72`),
    state: 1,
    direction: 'iddle',
    imgSrc : {
        happy : './src/img/slime_happy.svg',
        sad : './src/img/slime_sad.svg',
        woui : './src/img/slime_woui.svg',
        me : './src/img/slime_me.svg'
    }
}


const playerSound = new Audio('./src/sound/pop-2.mp3');

// LISTENER
document.getElementById('title').addEventListener("click", function() {
    toggleModal('title');
});

document.addEventListener("click", function(event){
    if (isModalOpen && event.target.id == "modal-overlay") {
        toggleModal(currModal);
        removeModalOverlay()
    }
});

document.getElementById('open-message').addEventListener("click", function() {
    toggleModal('message');

});
document.getElementById('open-login').addEventListener("click", function() {
    toggleModal('login');

});

var btnListener = ['leftButton', 'rightButton', 'startButton', 'playButton', 'pauseButton', 'loginButton', 'aiButton', 'soundButton', 'popButton'];

for (btn of btnListener) {
    let elem = document.getElementById(btn);
    elem.addEventListener("click", window[btn]);
}

document.getElementsByClassName('title')[0].classList.add('anim');

// MODAL
function toggleModal(modal) {

    if (modal === 'title') {
        closeModalTitle(true)
    }else {
        let elem  =
        document.getElementsByClassName(modal)[0];

        if (!elem.classList.contains("open") && isPopMessage) {
            removePop()
        }

        if (window.innerWidth < smallScreen) {
            elem.classList.toggle("open");
        }

        if (!isModalOpen) {
            toggleMainModal(true);
        }else {
            if (modal == currModal || window.innerWidth > smallScreen ) {
                toggleMainModal(false);
                return
            }else {
                if (currModal != 'none') {
                    document.getElementsByClassName(currModal)[0].classList.toggle("open");
                }
            }
        }
        currModal = modal;
    }
}

function toggleMainModal(open) {
    if (open) {
        let title = document.getElementById('title');
        let overlay = document.createElement('div');
        overlay.setAttribute('id', 'modal-overlay');
        document.getElementsByClassName('main-container')[0].insertBefore(overlay, title);
        isModalOpen = true;
    }else {
        removeModalOverlay();
        isModalOpen = false;
        currModal = "none";
    }
    document.getElementsByClassName('modal')[0].classList.toggle("open");
}

function closeModalTitle(isOpen) {
    let elem  = document.getElementById('title');

    if (isOpen) {
        elem.classList.toggle("open");
        setTimeout(function() {
            closeModalTitle(false);
        }, 300);
    }else {
        elem.style.display = "none";

    }
}

function removeModalOverlay() {
    let overlay = document.getElementById('modal-overlay');
    overlay.classList.add('fadeOut');
    setTimeout(function() {
        overlay.remove();
    }, 1000)
}

function toggleDemo() {
    let demo = document.getElementById('demo');
    if (isPlayerInLife) {
        demo.style.visibility = "hidden"
    }else {
        demo.style.visibility = "visible"
    }
}

// GAME UPDATE
game.on('matrix', (matrix) => {

    // AI FULL DEBILE
    let arrayToCheck = [];
    arrayToCheck.push(matrix[4], matrix[5], matrix[6]);

    for (var i = 0; i < matrix.length; i++) {
        for (var j = 0; j < matrix[i].length; j++) {
            let cell;
            if (matrix[i][j] == 1) {
                cell = document.getElementById(`${i}${j}`);
                cell.src = goutte.imgSrc.default;
            }else {
                cell = document.getElementById(`${i}${j}`);
                cell.src = goutte.imgSrc.empty;
            }

            // AI ON LINE 6

            if (i == 6 && isAi) {
                checkLine(arrayToCheck);
            }

            if (i == 7) {
                cell = document.getElementById(`${i}${j}`);
                removeClass(cell);

                if (j == game.position) {
                    if (isPlayerInLife) {
                        slime.state = 1
                    }else {
                        slime.state = 0
                    }
                    slime.elem = cell;
                    changeImgSrc();
                }else if (matrix[i][j] == 1) {
                    cell.classList.add('anim')
                    cell.src = goutte.imgSrc.anim;
                }
            }
        }
    }
})

// GAME OVER
game.on('sweaty', () => {
    console.log('GameOver');
    switchBtnInGame();
    isPlayerInLife = false;
    toggleDemo();
    if (isSoundActive) {
        var over = new Audio('./src/sound/game-over.mp3');
        over.play();
    }
})


// BUTTONS
function startButton() {
    game.start();
    isPlayerInLife = true;
    switchBtnInGame('play');
    toggleDemo();
    if (isSoundActive) {
        var start = new Audio('./src/sound/start.mp3');
        start.play();
    }
    if (isAi) {
        isAi = false;
        toggleAi(false);
    }
}

function playButton() {
    game.play();
    switchBtnInGame('play');
}

function pauseButton() {
    game.pause();
    switchBtnInGame('pause');
}

// bouton vers la gauche
function leftButton() {
    slime.elem.src = goutte.imgSrc.empty;
    removeClass(slime.elem);
    game.left()
    changeImgSrc();
    slime.direction = 'left';
}

// bouton vers la droite
function rightButton() {
    slime.elem.src = goutte.imgSrc.empty;
    removeClass(slime.elem);
    game.right()
    changeImgSrc();
    slime.direction = 'right';
}

// bouton login
function loginButton() {
    game.login();
}

// bouton login
function aiButton() {
    isAi = !isAi;
    toggleAi(isAi);
}

function toggleAi(runAi) {
    let ai = document.getElementById('aiButton');
    if (runAi) {
        ai.innerHTML = "Désactiver IA";
    }else {
        ai.innerHTML = "Activer IA";
    }
}

function soundButton() {
    isSoundActive = !isSoundActive;
    let btn = document.getElementById('soundButton');
    if (isSoundActive) {
        btn.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>Désactiver le son</title><path d="M5 17h-5v-10h5v10zm2-10v10l9 5v-20l-9 5zm17 4h-5v2h5v-2zm-1.584-6.232l-4.332 2.5 1 1.732 4.332-2.5-1-1.732zm1 12.732l-4.332-2.5-1 1.732 4.332 2.5 1-1.732z"/></svg>`;
    }else {
        btn.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>Activer le son</title><path d="M22 1.269l-18.455 22.731-1.545-1.269 3.841-4.731h-1.827v-10h4.986v6.091l2.014-2.463v-3.628l5.365-2.981 4.076-5.019 1.545 1.269zm-10.986 15.926v.805l8.986 5v-16.873l-8.986 11.068z"/></svg>`;
    }
}

function popButton() {
    isPopActive = !isPopActive;
    let btn = document.getElementById('popButton');
    if (isPopActive) {
        btn.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M22 3v13h-11.643l-4.357 3.105v-3.105h-4v-13h20zm2-2h-24v16.981h4v5.019l7-5.019h13v-16.981zm-5 6h-14v-1h14v1zm0 2h-14v1h14v-1zm-6 3h-8v1h8v-1z"/></svg>`;
    }else {
        btn.innerHTML = `<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M3.439 3l-1.439-1.714 1.532-1.286 17.382 20.714-1.533 1.286-2.533-3.019h-5.848l-7 5.02v-5.02h-4v-15.981h3.439zm11.747 14l-10.068-11.999h-3.118v11.999h4v3.105l4.357-3.105h4.829zm8.814 1.981h-2.588l-1.662-1.981h2.25v-11.999h-12.319l-1.679-2.001h15.998v15.981z"/></svg>`;
    }
}

// MESSAGE
// envoi un message (version en ligne)
function sendMessage() {
    var message = document.getElementById('messageInput').value
    if (game.sendMessage(message)) {
        // affiche pour moi
        displayMessage('moi', message, true)
        // reset l'input
        document.getElementById('messageInput').value = null
    }

}

// affiche un message
function displayMessage(author, message, isMe = false) {
    var display = document.getElementsByClassName('message-display')[0];
    console.log(display);

    var name;
    var element = document.createElement("div")
    var subEl = null
    if (isMe) {
        name = `<b>${author}</b>`
    } else {
        name = `<span>${author}</span>`
    }

    var elem = `<div class="message">${name} : ${message}</div>`;
    display.innerHTML += elem;
    display.scrollTop = display.scrollHeight;

    if (isPopActive && !isModalOpen && !isPopMessage) {
        let pop = document.getElementsByClassName('pop-container')[0];
        pop.innerHTML = elem;
        document.getElementById('pop-message').classList.add("pop");
        isPopMessage = true;
        setTimeout(removePop, 5000)
    }

}

function removePop() {
    let popCont = document.getElementsByClassName('pop-container')[0];
    document.getElementById('pop-message').classList.remove("pop");
    isPopMessage = false;
}

// nouveau message
game.on('message', (messageData) => {
    console.log('message', messageData)
    displayMessage(messageData.author, messageData.message)
})

function switchOnline() {
    let elems;
    if (isOnline) {
        elems = document.getElementsByClassName('offline');
    }else {
        elems = document.getElementsByClassName('online');
    }
    for (elem of elems) {
        elem.classList.add('none');
    }
}

function switchBtnInGame(state) {
    let play = document.getElementById('playButton');
    let pause = document.getElementById('pauseButton');
    switch (state) {
        case 'play':
            if (isOnline) {
                start.classList.toggle('pulse');
                start.setAttribute("disabled", "");
            }else {
                start.style.display = "none";
                pause.style.display = "block";
                play.style.display = "none";

            }
            break;
        case 'pause':
            pause.style.display = "none";
            play.style.display = "block";
            break;
        default:
            start.style.display = "block";
            if (isOnline && start.hasAttribute("disabled")) {
                start.removeAttribute("disabled");
                start.classList.toggle('pulse');
            }
            pause.style.display = "none";
            play.style.display = "none";

    }
}

// CHANGE IMG
function changeImgSrc() {
    if (slime.state == 1) {
        slime.elem.src = slime.imgSrc.happy;
    }else {
        slime.elem.src = slime.imgSrc.sad;
    }
    slime.elem.classList.add("player", slime.direction);
    if (slime.direction != 'iddle') {
        if (isSoundActive) {
            playerSound.play();
        }
        slime.elem.src = slime.imgSrc.me;
        setTimeout(() =>{
            slime.elem.src = slime.imgSrc.woui;
        },250);
        setTimeout(() =>{
            if (slime.state == 1) {
                slime.elem.src = slime.imgSrc.happy;
            }else {
                slime.elem.src = slime.imgSrc.sad;
            }
        },500);
    }
    slime.direction = 'iddle';
}

function removeClass(elem) {
    elem.classList.remove("player", "iddle","right", "left", "anim");
}
